<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {


	public function index()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('container');
		$this->load->view('layouts/footer');
	}
	public function mantenimiento()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('mantenimiento');
		$this->load->view('layouts/footer');
	}
	public function login()
	{
		$this->load->view('login');
	}







	public function listaProducto()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$data['producto']=$this->producto_model->retornarProducto();
		$this->load->view('admin/producto/lista',$data);
		$this->load->view('layouts/footer');
	}
	
	 
	public function agregar()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/producto/agregarForm');
		$this->load->view('layouts/footer');
	}

	public function agregardb()
	{
		$codigo=$_POST['codigo'];
		$data['codigo']=$codigo;
		
		$nombreProducto=$_POST['nombreProducto'];
		$data['nombreProducto']=$nombreProducto;
		
		$descripcion=$_POST['descripcion'];
		$data['descripcion']=$descripcion;
		
		$precio=$_POST['precio'];
		$data['precio']=$precio;
		
		$stock=$_POST['stock'];
		$data['stock']=$stock;

		$this->producto_model->agregarProducto($data);
		redirect('Welcome/listaProducto','refresh');
		//$this->load->view('layouts/header');
		//$this->load->view('layouts/aside');
		//$this->load->view('admin/producto/agregarForm');
		//$this->load->view('layouts/footer');

	}
	public function modificar()
	{
		$idProducto=$_POST['idProducto'];

		$data['codigo']=$this->producto_model->recuperarProducto($idProducto);
		$data['nombreProducto']=$this->producto_model->recuperarProducto($idProducto);
		$data['descripcion']=$this->producto_model->recuperarProducto($idProducto);
		$data['precio']=$this->producto_model->recuperarProducto($idProducto);
		$data['stock']=$this->producto_model->recuperarProducto($idProducto);
		


		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/producto/modificarForm',$data);
		$this->load->view('layouts/footer');
	}




	public function modificardb()
	{
		$idProducto=$_POST['idProducto'];

		$codigo=$_POST['codigo'];
		$data['codigo']=$codigo;
		
		$nombreProducto=$_POST['nombreProducto'];
		$data['nombreProducto']=$nombreProducto;
		
		$descripcion=$_POST['descripcion'];
		$data['descripcion']=$descripcion;
		
		$precio=$_POST['precio'];
		$data['precio']=$precio;
		
		$stock=$_POST['stock'];
		$data['stock']=$stock;

		$this->producto_model->modificarProducto($idProducto,$data);
		redirect('Welcome/listaProducto','refresh');
		//$this->load->view('head');
		//$this->load->view('modificarpaismensaje',$data);
		//$this->load->view('footer');
	}

	public function eliminardb()
	{
		$idProducto=$_POST['idProducto'];

		$codigo=$_POST['codigo'];
		$data['codigo']=$codigo;
		
		$nombreProducto=$_POST['nombreProducto'];
		$data['nombreProducto']=$nombreProducto;
		
		$descripcion=$_POST['descripcion'];
		$data['descripcion']=$descripcion;
		
		$precio=$_POST['precio'];
		$data['precio']=$precio;
		
		$stock=$_POST['stock'];
		$data['stock']=$stock;

		$this->producto_model->eliminarProducto($idProducto,$data);
		redirect('Welcome/listaProducto','refresh');
		//$this->load->view('head');
		//$this->load->view('eliminarmensaje',$data);
		//$this->load->view('footer');

	}
























	////PARA USUARIOS

	public function listaa()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$data['usuario']=$this->usuario_model->retornarUsuario();
		$this->load->view('admin/usuario/lista',$data);
		$this->load->view('layouts/footer');
	}


	public function agregarr()
	{
		$data=array(
			'roles'=>$this->usuario_model->getRoles(),
		);
		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/usuario/agregarForm',$data);
		$this->load->view('layouts/footer');
	}

	public function agregardbb()
	{
		$ci=$_POST['ci'];
		$data['ci']=$ci;
		
		$primerApellido=ucwords($_POST['primerApellido']);
		$data['primerApellido']=$primerApellido;
		
		$segundoApellido=ucwords($_POST['segundoApellido']);
		$data['segundoApellido']=$segundoApellido;
		
		$nombre=ucwords($_POST['nombre']);
		$data['nombre']=$nombre;
		
		$telefono=$_POST['telefono'];
		$data['telefono']=$telefono;

		$direccion=ucfirst($_POST['direccion']);
		$data['direccion']=$direccion;

		$rol=$_POST['rol'];
		$data['idRoles']=$rol;


		$a=strtoupper(substr($nombre,0,1));
		$n=strtoupper($primerApellido);
		
		$userName=$a.$n;
		$data['userName']=$userName;

		
		
		$c=$ci;

		$password=md5($c);
		$data['password']=$password;


		$this->usuario_model->agregarUsuario($data);
		redirect('Welcome/listaa','refresh');
		//$this->load->view('layouts/header');
		//$this->load->view('layouts/aside');
		//$this->load->view('admin/producto/agregarForm');
		//$this->load->view('layouts/footer');

	}
	public function modificarr()
	{
		$data=array(
			'roles'=>$this->usuario_model->getRoles(),
		);
		//$data['roles']=$this->usuario_model->getRoles();

		$idUsuario=$_POST['idUsuario'];

		$data['ci']=$this->usuario_model->recuperarUsuario($idUsuario);
		$data['primerApellido']=$this->usuario_model->recuperarUsuario($idUsuario);
		$data['segundoApellido']=$this->usuario_model->recuperarUsuario($idUsuario);
		$data['nombre']=$this->usuario_model->recuperarUsuario($idUsuario);
		$data['telefono']=$this->usuario_model->recuperarUsuario($idUsuario);
		$data['direccion']=$this->usuario_model->recuperarUsuario($idUsuario);
		$data['idRoles']=$this->usuario_model->recuperarUsuario($idUsuario);


		$this->load->view('layouts/header');
		$this->load->view('layouts/aside');
		$this->load->view('admin/usuario/modificarForm',$data);
		$this->load->view('layouts/footer');
	}
 
	public function modificardbb()
	{
		$idUsuario=$_POST['idUsuario'];

		$ci=$_POST['ci'];
		$data['ci']=$ci;
		
		$primerApellido=ucwords($_POST['primerApellido']);
		$data['primerApellido']=$primerApellido;
		
		$segundoApellido=ucwords($_POST['segundoApellido']);
		$data['segundoApellido']=$segundoApellido;
		
		$nombre=ucwords($_POST['nombre']);
		$data['nombre']=$nombre;
		
		$telefono=$_POST['telefono'];
		$data['telefono']=$telefono;

		$direccion=ucfirst($_POST['direccion']);
		$data['direccion']=$direccion;

		$rol=$_POST['rol'];
		$data['idRoles']=$rol;


		$a=strtoupper(substr($nombre,0,1));
		$n=strtoupper($primerApellido);
		
		$userName=$a.$n;
		$data['userName']=$userName;

		
		
		$c=$ci;

		$password=md5($c);
		$data['password']=$password;


		$this->usuario_model->modificarUsuario($idUsuario,$data);
		redirect('Welcome/listaa','refresh');

	}
}
