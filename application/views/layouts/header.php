<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistema de Ventas | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?=base_url()?>assets/template/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/template/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/template/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/template/dist/css/skins/_all-skins.min.css">

    <!-- panel de usuario -->
	<link rel="stylesheet" href="<?=base_url()?>libreria/panell/css/normalize.css">
	<link rel="stylesheet" href="<?=base_url()?>libreria/panell/css/sweetalert2.css">
	<link rel="stylesheet" href="<?=base_url()?>libreria/panell/css/material.min.css">
	<link rel="stylesheet" href="<?=base_url()?>libreria/panell/css/material-design-iconic-font.min.css">
	<link rel="stylesheet" href="<?=base_url()?>libreria/panell/css/main.css">
	<script>window.jQuery || document.write('<script src="<?=base_url()?>libreria/panell/js/jquery-1.11.2.min.js"><\/script>')</script>
	<script src="<?=base_url()?>libreria/panell/js/sweetalert2.min.js" ></script>
	<script src="<?=base_url()?>libreria/panell/js/main.js" ></script>

    <!--para tablas -->
    <link rel="stylesheet" href="<?=base_url()?>assets/template/datatables.net-bs/css/dataTables.bootstrap.min.css">
    


</head>
<body class="hold-transition skin-red sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>S</b>L</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>SirlenLumition</b></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">-------------</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?=base_url()?>assets/template/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                <span class="hidden-xs">Rojas Lineth</span>
                                <span class="hidden-xs"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-body">
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <a href="<?=base_url()?>index.php/Welcome/login" class="fa fa-power-off">  Cerrar Sesion</a>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </li>
                            </ul>
                        </li>








                        <!-- User Account: style can be found in dropdown.less -->
                        
                       <!-- <li class="dropdown user user-menu"> /////mejorar este li
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?=base_url()?>assets/template/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                                <span class="hidden-xs">Rojas Lineth</span>
                                <li class="btn-exit" id="btn-exit">
                                    <i class="zmdi zmdi-power"></i>
                                    <div class="mdl-tooltip" for="btn-exit">LogOut</div>
                                </li>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-body">
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <a href="#"> Cerrar Sesion</a>
                                        </div>
                                    </div>
                                    <!-- /.row 
                                </li>
                            </ul>
                        </li>-->






                        <!--<div class="full-width navBar">

                            <nav class="navBar-options-list">
                                <ul class="list-unstyle">
                                    
                                    <li class="btn-exit" id="btn-exit">
                                        <i class="zmdi zmdi-power"></i>
                                        <div class="mdl-tooltip" for="btn-exit">LogOut</div>
                                    </li>
                                    <li class="text-condensedLight noLink" ><small>UserName</small></li>
                                    <li class="noLink">
                                        <figure>
                                            <img src="<?=base_url()?>libreria/panell/assets/img/avatar-male.png" alt="Avatar" class="img-responsive">
                                        </figure>
                                    </li>
                                </ul>
                            </nav>
                        </div>-->

                    </ul>
                </div>
            </nav>
        </header>