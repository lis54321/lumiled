<div class="content-wrapper">
            <section class="content-header">
                <h1>
                USUARIOS
                <small>Nuevo</small>
                </h1>
            </section>
            
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-3">
                                <?php echo form_open_multipart('Welcome/agregardbb'); ?>
                                   <div>
                                   
                                        <div class="alert alert-danger" id="msg-error" style="text-align:left;">
                                            <strong >¡Importante!</strong>Corregir los siguentes errores.
                                            <div class="list-errors"></div>
                                        </div>

                                   <form action="<?php echo base_url();?>Welcome/agregardbb" method='POST'>
                                    <div class=form-group>
                                        <label for="ci">C.I.: </label>
                                        <input type="text" name="ci" placeholder="Ingrese el C.I. del usuario.."class="form-control" id="ci" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="primerApellido">Primer Apellido: </label>
                                        <input type="text" name="primerApellido" placeholder="Ingrese el 1er apellido del usuario.."class="form-control" id="primerApellido" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="segundoApellido">Segundo Apellido: </label>
                                        <input type="text" name="segundoApellido" placeholder="Ingrese el 2do apellido del usuario.."class="form-control" id="segundoApellido" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="nombre">Nombre: </label>
                                        <input type="text" name="nombre" placeholder="Ingrese el nombre del usuario.."class="form-control" id="nombre" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="telefono">Telefono: </label>
                                        <input type="text" name="telefono" placeholder="Ingrese el telefono del usuario.."class="form-control" id="telefono" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="direccion">Dirección: </label>
                                        <input type="text" name="direccion" placeholder="Ingrese la direccion del usuario.."class="form-control" id="direccion" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="rol">Rol: </label>
                                        <select name="rol" class="form-control" id="rol" >
                                            <?php foreach($roles as $rol):?>
                                                <option value="<?php echo $rol->idRoles;?>"><?php echo $rol->nombre;?></option>
                                            <?php endforeach;?> 
                                        </select> 
                                    </div >
                                    
                                    <div>
                                        <button type="submit" class="btn btn-primary">Registrar</button>
                                        <button type="submit" class="btn btn-primary">Cancelar</button>
                                    </div >
                                
                                 </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>