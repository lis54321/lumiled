        <div class="content-wrapper">
            
            <section class="content-header">
                <h1>
                USUARIOS
                <small>Listados</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                 <?php echo form_open_multipart('Welcome/agregarr'); ?>
                                <button type="submit" class="btn btn-primary">Agregar Nuevo Usuario</button>
                                <?php echo form_close(); ?>
                                <!--<a href="producto/agregar" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span> Agregar Producto </a>-->
                            </div>
                        </div>
                        <hr>
                        <table id="example1" class="table table-bordered btn-hover">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>CI</th>
                                    <th>Primer Apellido</th>
                                    <th>Segundo Apellido</th>
                                    <th>Nombre</th>
                                    <th>Telefono</th>
                                    <th>Direccion</th>
                                    <th>Rol</th>
                                    <th>Opciones</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $indice=1;
                                foreach ($usuario->result() as $row) {
                                ?>
                                <tr>
                                    <td><?php echo $indice; ?></td>
                                    <td><?php echo $row->ci; ?></td>
                                    <td><?php echo $row->primerApellido; ?></td>
                                    <td><?php echo $row->segundoApellido; ?></td>
                                    <td><?php echo $row->nombre; ?></td>
                                    <td><?php echo $row->telefono; ?></td>
                                    <td><?php echo $row->direccion; ?></td>
                                    <td><?php echo $row->rol; ?></td>
                                    
                                    <td>
                                        <div class="btn-group">
                                            <?php echo form_open_multipart('Welcome/modificarr'); ?>
                                                <input type="hidden" name="idUsuario" value="<?php echo $row->idUsuario; ?>">
                                                <button type="submit" class="btn btn-primary"><span class="fa fa-pencil"></button>
                                            <?php echo form_close(); ?>
                                        </div>
                                        <div class="btn-group">
                                            <?php echo form_open_multipart('Welcome/eliminardbb'); ?>
                                             
                                             <input type="hidden" name="ci" value="<?php echo $row->ci; ?>">
                                            <input type="hidden" name="primerApellido" value="<?php echo $row->primerApellido; ?>">
                                            <input type="hidden" name="segundoApellido" value="<?php echo $row->segundoApellido; ?>">
                                            <input type="hidden" name="nombre" value="<?php echo $row->nombre; ?>">
                                            <input type="hidden" name="telefono" value="<?php echo $row->telefono; ?>">
                                            <input type="hidden" name="direccion" value="<?php echo $row->direccion; ?>">
                                            <input type="hidden" name="rol" value="<?php echo $row->rol; ?>">
                                            
                                            <button type="submit" class="btn btn-danger"><span class="fa fa-remove"></button>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </td>
                                    <td>
                                    <div class="btn-group">

                                    
                                <?php
                                $indice++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
       
