        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                USUARIOS
                <small>Editar</small>
                </h1>
            </section>
            
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-3">
                                
                                    <?php
                                    foreach ($ci->result() as $row) {
                                    ?>
                                    <?php echo form_open_multipart('Welcome/modificardbb'); ?>
                                    
                                <form action="<?php echo base_url();?>Welcome/modificardbb" method='POST'>
                                   
                                    <div class=form-group>
                                    <input type="hidden" name="idUsuario" value="<?php echo $row->idUsuario; ?>">
                                     </div >
                                    
                                    <div class=form-group>
                                        <label for="ci">C.I.: </label>
                                        <input type="text" name="ci" class="form-control" id="ci" value="<?php echo $row->ci; ?>">
                                    </div >
                                    <div class=form-group>
                                        <label for="primerApellido">Primer Apellido: </label>
                                        <input type="text" name="primerApellido" class="form-control" id="primerApellido" value="<?php echo $row->primerApellido; ?>">>
                                    </div >
                                    <div class=form-group>
                                        <label for="segundoApellido">Segundo Apellido: </label>
                                        <input type="text" name="segundoApellido" class="form-control" id="segundoApellido" value="<?php echo $row->segundoApellido; ?>">
                                    </div >
                                    <div class=form-group>
                                        <label for="nombre">Nombre: </label>
                                        <input type="text" name="nombre" class="form-control" id="nombre" value="<?php echo $row->nombre; ?>">
                                    </div >
                                    <div class=form-group>
                                        <label for="telefono">Telefono: </label>
                                        <input type="text" name="telefono" class="form-control" id="telefono" value="<?php echo $row->telefono; ?>">
                                    </div >
                                    <div class=form-group>
                                        <label for="direccion">Dirección: </label>
                                        <input type="text" name="direccion" class="form-control" id="direccion" value="<?php echo $row->direccion; ?>">
                                    </div >
                                    <div class=form-group>
                                        <label for="rol">Rol: </label>
                                        <select name="rol" class="form-control" id="rol" >
                                            <?php foreach($roles as $rol):?>
                                                <option value="<?php echo $rol->idRoles;?>"<?php echo $rol->idRoles==$usuario->idRoles ? "selected":"";?>><?php echo $rol->nombre;?></option>
                                            <?php endforeach;?> 
                                        </select> 
                                    </div >
                                    
                                    <div>
                                        <button type="submit" class="btn btn-primary">Registrar</button>
                                        <button type="submit" class="btn btn-primary">Cancelar</button>
                                    </div >
                                    <?php echo form_close(); ?>
                                    <?php
                                    }
                                    ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
