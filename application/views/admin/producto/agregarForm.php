        <div class="content-wrapper">
            <section class="content-header">
                <h1>
                REGISTRO DE PRODUCTOS
                <small>Nuevo</small>
                </h1>
            </section>
            <a href="<?php echo base_url();?>index.php/Welcome">Ir a listas</a>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">  
                        <div class="row">
                            <div class="col-md-3">
                                <?php echo form_open_multipart('Welcome/agregardb'); ?>
                                   <div>
                                   <form action="/action_page.php">
                                    <div class=form-group>
                                        <label for="codigo">Codigo: </label>
                                        <input type="text" name="codigo" placeholder="Ingrese el Código.."class="form-control" id="codigo" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="nombreProducto">Nombre: </label>
                                        <input type="text" name="nombreProducto" placeholder="Ingrese el nombre del producto.."class="form-control" id="nombreProducto" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="descripcion">Descripción: </label>
                                        <input type="text" name="descripcion" placeholder="Descripción del producto.."class="form-control" id="descripcion" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="precio">Precio: </label>
                                        <input type="text" name="precio" placeholder="Ingrese el precio del producto.."class="form-control" id="precio" ></input>
                                    </div >
                                    <div class=form-group>
                                        <label for="stock">Stock: </label>
                                        <input type="text" name="stock" placeholder="Ingrese el stock del producto.."class="form-control" id="stock" ></input>
                                    </div >
                                    <div>
                                        <button type="submit" class="btn btn-primary">Registrar</button>
                                        <button type="submit" class="btn btn-primary">Cancelar</button>
                                    </div >
                                
                                 </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
       
