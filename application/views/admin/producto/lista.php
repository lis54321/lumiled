        <div class="content-wrapper">
            
            <section class="content-header">
                <h1>
                PRODUCTOS
                <small>Listados</small>
                </h1>
            </section>
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="box box-solid">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                 <?php echo form_open_multipart('Welcome/agregar'); ?>
                                <button type="submit" class="btn btn-primary">Agregar Nuevo Producto</button>
                                <?php echo form_close(); ?>
                                <!--<a href="producto/agregar" class="btn btn-primary btn-flat"><span class="fa fa-plus"></span> Agregar Producto </a>-->
                            </div>
                        </div>
                        <hr>
                        <table id="example1" class="table table-bordered btn-hover">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>idCategoria</th>
                                    <th>Codigo</th>
                                    <th>Nombre</th>
                                    <th>Descripcion</th>
                                    <th>Precio</th>
                                    <th>Stock</th>
                                    <th>Opciones</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $indice=1;
                                foreach ($producto->result() as $row) {
                                ?>
                                <tr>
                                    <td><?php echo $indice; ?></td>
                                    <td><?php echo $row->idCategoria; ?></td>
                                    <td><?php echo $row->codigo; ?></td>
                                    <td><?php echo $row->nombreProducto; ?></td>
                                    <td><?php echo $row->descripcion; ?></td>
                                    <td><?php echo $row->precio; ?></td>
                                    <td><?php echo $row->stock; ?></td>
                                   
                                    <td>
                                        <div class="btn-group">
                                            <?php echo form_open_multipart('Welcome/modificar'); ?>
                                                <input type="hidden" name="idProducto" value="<?php echo $row->idProducto; ?>">
                                                <button type="submit" class="btn btn-primary"><span class="fa fa-pencil"></button>
                                            <?php echo form_close(); ?>
                                        </div>
                                        <div class="btn-group">
                                            <?php echo form_open_multipart('Welcome/eliminardb'); ?>
                                            <input type="hidden" name="idProducto" value="<?php echo $row->idProducto; ?>"></input>
                                            <input type="hidden" name="codigo" value="<?php echo $row->codigo; ?>"></input>
                                            <input type="hidden" name="nombreProducto" value="<?php echo $row->nombreProducto; ?>"></input>
                                            <input type="hidden" name="descripcion" value="<?php echo $row->descripcion; ?>"></input>
                                            <input type="hidden" name="precio" value="<?php echo $row->precio; ?>"></input>
                                            <input type="hidden" name="stock" value="<?php echo $row->stock; ?>"></input>
                                            <button type="submit" class="btn btn-danger"><span class="fa fa-remove"></button>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </td>
                                    <td>
                                    <div class="btn-group">
                                  
                                </tr>
                                <?php
                                $indice++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
            <!-- /.content -->
        </div>
       
