<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model {

	public function retornarUsuario()
	{
		$this->db->select('u.idUsuario,u.ci,u.primerApellido,u.segundoApellido,u.nombre,u.telefono,u.direccion,r.nombre as rol');
		$this->db->from('usuario u');
		$this->db->join('roles r','u.idRoles = r.idRoles');
		//$this->db->where('estado',1);
		return $this->db->get();
	}
 
	

 	public function getRoles()//para extraeer los datos de la otra tabla
	{
		$resultados=$this->db-> get('roles');
		return $resultados->result();
	}

	public function agregarUsuario($data)
	{
		$this->db->insert('usuario',$data);
	}
	
	public function recuperarUsuario($idUsuario)
	{
		$this->db->select('*');
		$this->db->from('usuario');
		$this->db->where('idUsuario',$idUsuario);
		return $this->db->get();
	}
	
	public function modificarUsuario($idUsuario,$data)
	{
		$this->db->where('idUsuario',$idUsuario);
		$this->db->update('usuario',$data);
	}
	
	public function eliminarUsuario($idUsuario)
	{
		$this->db->where('idUsuario',$idUsuario);
		$this->db->delete('usuario');
	}


	

}