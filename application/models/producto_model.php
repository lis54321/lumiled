<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producto_model extends CI_Model {

	public function retornarProducto()
	{
		$this->db->select('*');
		$this->db->from('producto');
		$this->db->where('estado',1);
		return $this->db->get();
	}
	public function agregarProducto($data)
	{
		$this->db->insert('producto',$data);
	}
	
	public function recuperarProducto($idProducto)
	{
		$this->db->select('*');
		$this->db->from('producto');
		$this->db->where('idProducto',$idProducto);
		return $this->db->get();
	}
	
	public function modificarProducto($idProducto,$data)
	{
		$this->db->where('idProducto',$idProducto);
		$this->db->update('producto',$data);
	}
	
	public function eliminarProducto($idProducto)
	{
		$this->db->where('idProducto',$idProducto);
		$this->db->delete('producto');
	}

}